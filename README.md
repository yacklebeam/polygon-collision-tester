# Program for creating "maps" of 2d polygons for collision testing.

**WATCH A DEMO HERE**: https://www.youtube.com/watch?v=r1_-AvcSOOc

## **CONTROLS**

**EDIT MODE**

Ctrl + Left Click : Add point

Alt + Left Click : Add hexagon under cursor

Hold Left Click : Move point under cursor

A : Convert current points to new polygon

Q : Toggle displaying bounding boxes

R : Toggle displaying quadtree

W, S : Switch selected polygon (yellow)

E : Deselect all polygons

Delete: Delete selected polygon

P : Switch to PLAY mode

M : Generate 100 random hexagons (for testing)

**PLAY MODE**

W, A, S, D : Move player

R : Toggle diplaying quadtree

P : Switch to EDIT mode

## **COLORS**

**EDIT MODE**

Red : Polygon, non-selected

Yellow : Polygon, selected

White : Polygon, possible collision to selected via quadtree

Green : Bounding box

Cyan : QuadTree boundary

Blue : Player

**PLAY MODE**

Red : Polygon, non-selected

White : Polygon, possible collision to player via quadtree

Cyan : QuadTree boundary

Blue : Player

Green : Polygon, bounding box collision with player