#ifndef POLY_QUADTREE
#define POLY_QUADTREE

#include <vector>
#include <SFML/Graphics.hpp>
#include <cstdio>

#define CHILD_COUNT 4
#define MAX_OBJECT_COUNT 4

namespace pct
{
class QuadTree
{
private:
	class QuadObject
	{
	public:
		int id;
		sf::Vector2f upperLeft;
		sf::Vector2f lowerRight;
		QuadObject(sf::Vector2f ul, sf::Vector2f lr, int newId)
		{
			id = newId;
			upperLeft = ul;
			lowerRight = lr;
		}
	};
	int maxObjects;
	std::vector<QuadObject> objects;
	bool hasChildren;
	std::vector<sf::Vertex> renderPoints;
	bool fitsInside(sf::Vector2f ul, sf::Vector2f lr);
	bool isSimpleRectangleCollision(sf::Vector2f A1, sf::Vector2f A2, sf::Vector2f B1, sf::Vector2f B2);
	QuadTree* children[CHILD_COUNT];

public:
	sf::Vector2f upperLeft;
	sf::Vector2f lowerRight;

	QuadTree();
	QuadTree(sf::Vector2f ul, sf::Vector2f lr);
	~QuadTree();

	bool insert(sf::Vector2f ul, sf::Vector2f lr, int id);
	void draw(sf::RenderWindow *window);

	std::vector<int> getPossibleCollisions(sf::Vector2f ul, sf::Vector2f lr);
};

}//ns

#endif