//TODO (jtroxel): convert to camera based scene and map
//TODO (jtroxel): smooth player controls (?)

#include <cstdio>
#include <windows.h>
#include <time.h>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "polygon.hpp"
#include "quadtree.hpp"
#include "input.hpp"

#include "collision.cpp"

enum GameMode {
    EDIT,
    PLAY,
};

bool
intersects(sf::Vector2f A1, sf::Vector2f A2, sf::Vector2f B1, sf::Vector2f B2)
{
   return (A1.x < B2.x && A2.x > B1.x && A1.y < B2.y && A2.y > B1.y);
}

std::vector<sf::Vertex>
createSimpleHegagon(float x, float y)
{
    std::vector<sf::Vertex> result;

    result.push_back(sf::Vertex(sf::Vector2f(x + 10, y), sf::Color::Red));                            
    result.push_back(sf::Vertex(sf::Vector2f(x + 30, y), sf::Color::Red));                            
    result.push_back(sf::Vertex(sf::Vector2f(x + 40, y + 20), sf::Color::Red));                            
    result.push_back(sf::Vertex(sf::Vector2f(x + 30, y + 40), sf::Color::Red));                            
    result.push_back(sf::Vertex(sf::Vector2f(x + 10, y + 40), sf::Color::Red));                            
    result.push_back(sf::Vertex(sf::Vector2f(x, y + 20), sf::Color::Red));                            

    return result;
}


#if 0
int
main()
#else
int WINAPI
WinMain(HINSTANCE hInstance,
        HINSTANCE hPrevInstance,
        LPSTR lpCmdLine,
        int nCmdShow)
#endif
{
    srand (time(NULL));
	sf::RenderWindow window(sf::VideoMode(1200, 800), "PCT");
    sf::Vector2f worldMaxCoords(4800.f, 3200.f);

	pct::QuadTree qtree(sf::Vector2f(0.f,0.f), worldMaxCoords);
	std::vector<sf::Vertex> newPoints;
	std::vector<pct::Polygon> polygons;

    pct::InputController icontroller(0);

    sf::Vector2f playerPosition(580, 380);
    std::vector<sf::Vertex> playerRenderPoints;

	bool showQTree = false;
	bool showRectangles = false;
	int selectedPolygon = -1;
	int curId = 0;

    int selectedPointPoly = -1;
    int selectedPointIndex = -1;

    sf::Vector2f cameraPosition(0.f, 0.f);
    sf::Vector2f cameraMaxCoords(1200.f,800.f);
    sf::Transform globalTransform;

    GameMode mode = EDIT;

    sf::Font font;
    if (!font.loadFromFile("../res/sansation.ttf"))
    {
        // error...
    }

    sf::Text text;
    text.setFont(font);
    text.setString("EDIT");
    text.setPosition(10.f, 10.f);
    text.setCharacterSize(18);
    text.setFillColor(sf::Color::White);

    std::vector<sf::Vertex> worldEdgeRenderPoints;
    worldEdgeRenderPoints.push_back(sf::Vertex(sf::Vector2f(0, 0), sf::Color::Blue));
    worldEdgeRenderPoints.push_back(sf::Vertex(sf::Vector2f(4800, 0), sf::Color::Blue));
    worldEdgeRenderPoints.push_back(sf::Vertex(sf::Vector2f(4800, 0), sf::Color::Blue));
    worldEdgeRenderPoints.push_back(sf::Vertex(sf::Vector2f(4800, 3200), sf::Color::Blue));
    worldEdgeRenderPoints.push_back(sf::Vertex(sf::Vector2f(4800, 3200), sf::Color::Blue));
    worldEdgeRenderPoints.push_back(sf::Vertex(sf::Vector2f(0, 3200), sf::Color::Blue));
    worldEdgeRenderPoints.push_back(sf::Vertex(sf::Vector2f(0, 3200), sf::Color::Blue));
    worldEdgeRenderPoints.push_back(sf::Vertex(sf::Vector2f(0, 0), sf::Color::Blue));

    
    while (window.isOpen())
    {
        sf::Event event;
        pct::InputCommand command;
        while(window.pollEvent(event))
        {
            command = icontroller.parseEvent(event);
            switch(command)
            {
                case pct::EXIT_GAME:
                {
                    window.close();
                } break;
                case pct::MOVE_PLAYER_UP:
                {
                    playerPosition.y -= 10;
                    cameraPosition.y -= 10;
                    cameraMaxCoords.y -= 10;
                    globalTransform.translate(0, 10);
                } break;
                case pct::MOVE_PLAYER_LEFT:
                {
                    playerPosition.x -= 10;
                    cameraPosition.x -= 10;
                    cameraMaxCoords.x -= 10;
                    globalTransform.translate(10, 0);
                } break;
                case pct::MOVE_PLAYER_DOWN:
                {
                    playerPosition.y += 10;
                    cameraPosition.y += 10;
                    cameraMaxCoords.y += 10;
                    globalTransform.translate(0, -10);
                } break;
                case pct::MOVE_PLAYER_RIGHT:
                {
                    playerPosition.x += 10;
                    cameraPosition.x += 10;
                    cameraMaxCoords.x += 10;
                    globalTransform.translate(-10, 0);
                } break;
                case pct::EDIT_GENERATE_MASS_HEXAGONS:
                {
                    for(int i = 0; i < 1000; ++i)
                    {
                        int x = rand() % ((int)worldMaxCoords.x - 200) + 100;
                        int y = rand() % ((int)worldMaxCoords.y - 200) + 100;

                        polygons.push_back(pct::Polygon(&createSimpleHegagon((float)x, (float)y), curId));
                        qtree.insert(polygons.back().getUpperLeft(), polygons.back().getLowerRight(), curId);
                        curId++;                                
                    }
                } break;
            }
        }
    



    
/*
	while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch(event.type)
            {
	            case sf::Event::Closed:
				{
	                window.close();
				} break;
	            case sf::Event::MouseButtonPressed:
	            {
                    if(mode == EDIT)
                    {
                        if (event.mouseButton.button == sf::Mouse::Left)
                        {
                            sf::Vector2i localP = sf::Mouse::getPosition(window);

                            if(sf::Keyboard::isKeyPressed(sf::Keyboard::LControl))
                            {
                                newPoints.push_back(sf::Vertex(sf::Vector2f(localP.x, localP.y), sf::Color::Red));                            
                            }
                            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt))
                            {
                                
                                polygons.push_back(pct::Polygon(&createSimpleHegagon(localP.x, localP.y), curId));
                                qtree.insert(polygons.back().getUpperLeft(), polygons.back().getLowerRight(), curId);
                                curId++;
                            }
                            else
                            {
                                //find the point we are clicking
                                bool found = false;
                                for(int i = 0; i < polygons.size(); ++i)
                                {
                                    for(int pointi = 0; pointi < polygons[i].points.size(); ++pointi)
                                    {
                                        float xdist = localP.x - polygons[i].points[pointi].x;
                                        float ydist = localP.y - polygons[i].points[pointi].y;
                                        float dist = sqrt(xdist * xdist + ydist * ydist);
                                        if(dist < 5.f)
                                        {
                                            //select this point
                                            selectedPointPoly = i;
                                            selectedPointIndex = pointi;
                                            found = true;
                                            break;
                                        }
                                    }
                                    if(found) break;
                                }
                            }
                        }
                    }
	            } break;
                case sf::Event::MouseButtonReleased:
                {
                    selectedPointPoly = -1;
                    selectedPointIndex = -1;
                } break;
                case sf::Event::MouseMoved:
                {
                    if(mode == EDIT)
                    {
                        if(selectedPointPoly >= 0 && selectedPointIndex >= 0)
                        {
                            polygons[selectedPointPoly].points[selectedPointIndex].x = event.mouseMove.x;
                            polygons[selectedPointPoly].points[selectedPointIndex].y = event.mouseMove.y;
                            polygons[selectedPointPoly].updateRenderPoints();
                        }
                    }
                } break;
	            case sf::Event::KeyPressed:
	            {
                    if(mode == EDIT)
                    {
    	            	if(event.key.code == sf::Keyboard::A)
    	            	{
    	            		polygons.push_back(pct::Polygon(&newPoints, curId));
    	            		qtree.insert(polygons.back().getUpperLeft(), polygons.back().getLowerRight(), curId);
    	            		curId++;
    	            		newPoints.clear();
    	            	}
    	            	if(event.key.code == sf::Keyboard::Q)
    	            	{
    	            		showRectangles = !showRectangles;
    	            	}
    	            	if(event.key.code == sf::Keyboard::Delete)
    	            	{
    	            		if(polygons.size() > 0 && selectedPolygon >= 0) polygons.erase(polygons.begin() + selectedPolygon);
    	            		selectedPolygon = -1;
    	            	}
    	            	if(event.key.code == sf::Keyboard::W)
    	            	{
    	            		selectedPolygon++;
    	            		if(selectedPolygon == polygons.size()) selectedPolygon = 0;

    	            	}
    	            	if(event.key.code == sf::Keyboard::S)
    	            	{
    	            		selectedPolygon--;
    	            		if(selectedPolygon == -1) selectedPolygon = polygons.size() - 1;
    	            	}
    	            	if(event.key.code == sf::Keyboard::E)
    	            	{
    	            		selectedPolygon = -1;
    	            	}
    	            	if(event.key.code == sf::Keyboard::R)
    	            	{
    	            		showQTree = !showQTree;
    	            	}
                        if(event.key.code == sf::Keyboard::P)
                        {
                            mode = PLAY;
                            showRectangles = false;
                            showQTree = false;
                            selectedPolygon = -1;
                            text.setString("PLAY");
                        }
                        if(event.key.code == sf::Keyboard::M)
                        {
                            for(int i = 0; i < 1000; ++i)
                            {
                                int x = rand() % ((int)worldMaxCoords.x - 200) + 100;
                                int y = rand() % ((int)worldMaxCoords.y - 200) + 100;

                                polygons.push_back(pct::Polygon(&createSimpleHegagon((float)x, (float)y), curId));
                                qtree.insert(polygons.back().getUpperLeft(), polygons.back().getLowerRight(), curId);
                                curId++;                                
                            }
                        }
                    }
                    else if(mode == PLAY)
                    {
                        if(event.key.code == sf::Keyboard::W)
                        {
                            playerPosition.y -= 10;
                            cameraPosition.y -= 10;
                            cameraMaxCoords.y -= 10;
                            globalTransform.translate(0, 10);
                        }
                        if(event.key.code == sf::Keyboard::A)
                        {
                            playerPosition.x -= 10;
                            cameraPosition.x -= 10;
                            cameraMaxCoords.x -= 10;
                            globalTransform.translate(10, 0);
                        }
                        if(event.key.code == sf::Keyboard::S)
                        {
                            playerPosition.y += 10;
                            cameraPosition.y += 10;
                            cameraMaxCoords.y += 10;
                            globalTransform.translate(0, -10);
                        }
                        if(event.key.code == sf::Keyboard::D)
                        {
                            playerPosition.x += 10;
                            cameraPosition.x += 10;
                            cameraMaxCoords.x += 10;
                            globalTransform.translate(-10, 0);
                        }
                        if(event.key.code == sf::Keyboard::P)
                        {
                            mode = EDIT;
                            text.setString("EDIT");
                        }
                        if(event.key.code == sf::Keyboard::R)
                        {
                            showQTree = !showQTree;
                        }
                    }
	            } break;
            }
        }
*/
        for(int i = 0; i < polygons.size(); ++i)
        {
        	polygons[i].deselect();
        }

        /*if(mode == EDIT && selectedPolygon >= 0)
        {
        	polygons[selectedPolygon].select();

        	std::vector<int> possibleCollisions = qtree.getPossibleCollisions(	polygons[selectedPolygon].getUpperLeft(), 
        																		polygons[selectedPolygon].getLowerRight());
        	for(int i = 0; i < possibleCollisions.size(); ++i)
        	{
        		if(possibleCollisions[i] != selectedPolygon) polygons[possibleCollisions[i]].highlight();
        	}
        }*/

        //move player to polygon, don't rely on this one check, but do all?
        //if(mode == PLAY)
        {
            std::vector<int> possiblePlayerCollisions = qtree.getPossibleCollisions(playerPosition, 
                                                                                    sf::Vector2f(playerPosition.x + 40.0f, playerPosition.y + 40.0f));

            text.setString("PLAY [" + std::to_string(possiblePlayerCollisions.size()) +"/" + std::to_string(polygons.size()) + "]");
            for(int i = 0; i < possiblePlayerCollisions.size(); ++i)
            {
                int polygonIndex = possiblePlayerCollisions[i];

                polygons[polygonIndex].highlight();
                if(intersects(playerPosition, sf::Vector2f(playerPosition.x + 40.0f, playerPosition.y + 40.0f), polygons[polygonIndex].getUpperLeft(), polygons[polygonIndex].getLowerRight()))
                {
                    polygons[polygonIndex].extraHighlight();
                }

                // CollisionResult_t result = getCollision(&(polygons[polygonIndex].points),
                // 										&(polygons[polygonIndex].points),
                // 										sf::Vector2f(10, 0));
                // if(result.tMax < 1.0)
                // {
                // 	//actual intersection, so light it up
                //     polygons[polygonIndex].extraHighlight();
                // }
            }
        }

        //build player, this is for testing and should be moved/made more elegant if possible
        playerRenderPoints.clear();
        playerRenderPoints.push_back(sf::Vertex(sf::Vector2f(playerPosition.x, playerPosition.y), sf::Color::Blue));
        playerRenderPoints.push_back(sf::Vertex(sf::Vector2f(playerPosition.x + 40.0f, playerPosition.y), sf::Color::Blue));
        playerRenderPoints.push_back(sf::Vertex(sf::Vector2f(playerPosition.x + 40.0f, playerPosition.y), sf::Color::Blue));
        playerRenderPoints.push_back(sf::Vertex(sf::Vector2f(playerPosition.x + 40.0f, playerPosition.y + 40.0f), sf::Color::Blue));
        playerRenderPoints.push_back(sf::Vertex(sf::Vector2f(playerPosition.x + 40.0f, playerPosition.y + 40.0f), sf::Color::Blue));
        playerRenderPoints.push_back(sf::Vertex(sf::Vector2f(playerPosition.x, playerPosition.y + 40.0f), sf::Color::Blue));
        playerRenderPoints.push_back(sf::Vertex(sf::Vector2f(playerPosition.x, playerPosition.y + 40.0f), sf::Color::Blue));
        playerRenderPoints.push_back(sf::Vertex(sf::Vector2f(playerPosition.x, playerPosition.y), sf::Color::Blue));
        //end build player

        window.clear();
        //if(newPoints.size() > 0) window.draw(&newPoints[0], newPoints.size(), sf::Points);
        for(int i = 0; i < polygons.size(); ++i)
        {
        	if(intersects(cameraPosition, cameraMaxCoords, polygons[i].getUpperLeft(), polygons[i].getLowerRight()))
        	{
	        	polygons[i].draw(&window, showRectangles, globalTransform);
        	}
        }
        //if(showQTree) qtree.draw(&window, globalTransform);
        window.draw(&playerRenderPoints[0], playerRenderPoints.size(), sf::Lines, globalTransform);
        window.draw(text);
        window.draw(&worldEdgeRenderPoints[0], worldEdgeRenderPoints.size(), sf::Lines, globalTransform);
        window.display();
    }


	return 0;
}