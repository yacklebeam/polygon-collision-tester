#include "polygon.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

struct CollisionResult_t
{
	float tMax;
	sf::Vector2f normal;
};

float
operator^(sf::Vector2f A, sf::Vector2f B)
{
	return A.x * B.x + A.y * B.y;
}

sf::Vector2f
operator-(sf::Vector2f input)
{
	sf::Vector2f result;
	result.x = -input.x;
	result.y = -input.y;
	return result;
}

float
maxFloat(float A, float B)
{
	return (A>B)?A:B;
}

// Finds t1 (or t2) between p1 + t1(p2) and p3 + t2(p4)
float
getIntersection(sf::Vector2f p1, sf::Vector2f p2, sf::Vector2f p3, sf::Vector2f p4)
{
	float result = 0.0f;
    float numerator   = ((p4.x - p3.x) * (p1.y - p3.y)) - ((p4.y - p3.y) * (p1.x - p3.x));
    float denominator = ((p4.y - p3.y) * (p2.x - p1.x)) - ((p4.x - p3.x) * (p2.y - p1.y));
    if(denominator == 0)
    {
        return 1.0f;
    }
    result = numerator / denominator;
    return result;	
}

// Uses dot product to determine if angle between vectors is acute
bool
isAligned(sf::Vector2f A, sf::Vector2f B)
{
	return ((A^B) >= 0);
}

// Returns orthagonal vector to D on opposite side of dAway
sf::Vector2f
getPerpindicularAwayFrom(sf::Vector2f D, sf::Vector2f dAway)
{
	sf::Vector2f newD(D.x, -D.y);
	if(isAligned(newD, dAway))
	{
		newD = -newD;
	}
	return newD;
}

// Returns normalized A
sf::Vector2f
normalize(sf::Vector2f A)
{
    float length = std::sqrt(A.x * A.x + A.y * A.y);
    sf::Vector2f Result;
    Result.x = A.x / length;
    Result.y = A.y / length;
    return(Result);
}

// Determines if X and Y are on opposite sides of Line
bool
aroundLine(sf::Vector2f X, sf::Vector2f Y, sf::Vector2f Line)
{
    float A = Line.y;
    float B = -Line.x;

    float xResult = A * X.x + B * X.y;
    float yResult = A * Y.x + B * Y.y;

    if(xResult == 0 || yResult == 0) return false;
    if(xResult < 0 && yResult > 0) return true;
    if(xResult > 0 && yResult < 0) return true;
    else return false;
}

// Returns index of points in max direction D in A
int
findMax(std::vector<sf::Vector2f>* A, sf::Vector2f D)
{
    float maxVal = FLT_MIN;
    int maxI = -1;
    for(int i = 0; i < A->size(); ++i)
    {
        if((D^A->at(i)) > maxVal)
        {
            maxVal = (D^A->at(i));
            maxI = i;
        }
    }
    return maxI;
}

// Find the max Minkowski Sum point in direction D
sf::Vector2f
support(std::vector<sf::Vector2f>* A, std::vector<sf::Vector2f>* B, sf::Vector2f D)
{
    sf::Vector2f Result;
    int indexA = findMax(A, D);
    int indexB = findMax(B, -D);
    Result = A->at(indexA) - B->at(indexB);
    return Result;
}

// Converging Collision Detection, calculates min distance
CollisionResult_t 
getCollision(std::vector<sf::Vector2f>* X, std::vector<sf::Vector2f>* Y, sf::Vector2f D)
{
    sf::Vector2f Simplex[2];
    sf::Vector2f A = support(X,Y,D);
    Simplex[0] = A;
    sf::Vector2f perpD = getPerpindicularAwayFrom(D, A);
    sf::Vector2f B = support(X,Y,perpD);
    Simplex[1] = B;
    int count = 0;
    bool infinite = false;
    while(count < (X->size() * Y->size()))
    {
        sf::Vector2f newDir = getPerpindicularAwayFrom(Simplex[1] - Simplex[0], -D);
        sf::Vector2f C = support(X, Y, newDir);
        if(C == Simplex[1] || C == Simplex[0])
        {
            break;
        }
        if(aroundLine(Simplex[0],C,-D))
        {
            Simplex[1] = C;
        }
        else if(aroundLine(Simplex[1],C,-D))
        {
            Simplex[0] = C;
        }
        else //neither line segment intersects, so this guy is infinite.
            //TODO (jtroxel): Check this for robustness
        {
            infinite = true;
            break;
        }
        ++count;
    }
    CollisionResult_t Result;
    Result.normal = normalize(getPerpindicularAwayFrom(Simplex[0] - Simplex[1], D));
    if(!infinite)
    {
        //instead of using line, we use the parallel line kEpsilon away from the line
        float testT = getIntersection(sf::Vector2f(0,0),-D, Simplex[0], Simplex[1]);
        sf::Vector2f k_EpsilonVector = Result.normal * (1.0f - 0.001f);
        sf::Vector2f Point1 = Simplex[0] - k_EpsilonVector;
        sf::Vector2f Point2 = Simplex[1] - k_EpsilonVector;
        Result.tMax = getIntersection(sf::Vector2f(0,0),-D, Point1, Point2);
        if(testT > 0 && Result.tMax < 0)
        {
            Result.tMax = 0.0f;
        }
    }
    else Result.tMax = 1.0f;
    return(Result);
}