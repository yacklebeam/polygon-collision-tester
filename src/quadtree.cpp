#include "quadtree.hpp"

using namespace pct;

QuadTree::QuadTree()
{
	hasChildren = false;
	maxObjects = MAX_OBJECT_COUNT;
}

QuadTree::QuadTree(sf::Vector2f ul, sf::Vector2f lr)
{
	upperLeft = ul;
	lowerRight = lr;
	hasChildren = false;
	maxObjects = MAX_OBJECT_COUNT;

	renderPoints.push_back(sf::Vertex(upperLeft, sf::Color::Cyan));
	renderPoints.push_back(sf::Vertex(sf::Vector2f(lowerRight.x, upperLeft.y), sf::Color::Cyan));
	renderPoints.push_back(sf::Vertex(sf::Vector2f(lowerRight.x, upperLeft.y), sf::Color::Cyan));
	renderPoints.push_back(sf::Vertex(lowerRight, sf::Color::Cyan));
	renderPoints.push_back(sf::Vertex(lowerRight, sf::Color::Cyan));
	renderPoints.push_back(sf::Vertex(sf::Vector2f(upperLeft.x, lowerRight.y), sf::Color::Cyan));
	renderPoints.push_back(sf::Vertex(sf::Vector2f(upperLeft.x, lowerRight.y), sf::Color::Cyan));
	renderPoints.push_back(sf::Vertex(upperLeft, sf::Color::Cyan));
}

QuadTree::~QuadTree()
{

}

bool
QuadTree::insert(sf::Vector2f ul, sf::Vector2f lr, int id)
{
	if(!fitsInside(ul, lr))
	{
		return false;
	}
	bool inserted = false;

	if(hasChildren)
	{
		//we always try the children first
		for(int i = 0; i < CHILD_COUNT; ++i)
		{
			inserted = children[i]->insert(ul, lr, id);
			if(inserted)
			{
				return true;
			}
		}
	}

	if(!inserted)
	{
		//no children could take it, so keep it here?
		//we may have more than max, but that's ok.
		objects.push_back(QuadObject(ul, lr, id));
		inserted = true;

		if(objects.size() > maxObjects && !hasChildren)
		{
			//rebuild this node
			//first, create children QTrees if necessary
			// sf::Vector2f midMid = (upperLeft + lowerRight) / 2.0f;
			// sf::Vector2f rightMid(lowerRight.x, midMid.y);
			// sf::Vector2f leftMid(upperLeft.x, midMid.y);
			// sf::Vector2f upperMid(midMid.x, upperLeft.y);
			// sf::Vector2f lowerMid(midMid.x, lowerRight.y);
			
			// children[0] = new QuadTree(upperLeft, midMid);
			// children[1] = new QuadTree(upperMid, rightMid);
			// children[2] = new QuadTree(leftMid, lowerMid);
			// children[3] = new QuadTree(midMid, lowerRight);

			int sqrtChildCount = sqrt(CHILD_COUNT);
			float childWidth = (lowerRight.x - upperLeft.x) / sqrtChildCount;
			float childHeight = (lowerRight.y - upperLeft.y) / sqrtChildCount;

			for(int col = 0; col < sqrtChildCount; ++col)
			{
				for(int row = 0; row < sqrtChildCount; ++row)
				{
					children[(row * sqrtChildCount) + col] = new QuadTree(sf::Vector2f(	upperLeft.x + (childWidth * col), 
																						upperLeft.y + (childHeight * row)),
																		  sf::Vector2f( upperLeft.x + (childWidth * (col + 1)), 
																						upperLeft.y + (childHeight * (row + 1))));
				}
			}


			hasChildren = true;

			//rebuild the tree
			std::vector<QuadObject> copiedObjects;
			for(int i = 0; i < objects.size(); ++i)
			{
				copiedObjects.push_back(objects[i]);
			}
			objects.clear();
			for(int i = 0; i < copiedObjects.size(); ++i)
			{
				QuadObject qo = copiedObjects[i];
				insert(qo.upperLeft, qo.lowerRight, qo.id);
			}
		}
	}

	return inserted;
}

void
QuadTree::draw(sf::RenderWindow *window)
{
	window->draw(&renderPoints[0], renderPoints.size(), sf::Lines);
	if(hasChildren)
	{
		for(int i = 0; i < CHILD_COUNT; ++i)
		{
			children[i]->draw(window);
		}
	}
}

bool
QuadTree::fitsInside(sf::Vector2f ul, sf::Vector2f lr)
{
	return !(ul.x < upperLeft.x || lr.x > lowerRight.x || ul.y < upperLeft.y || lr.y > lowerRight.y);
}

bool
QuadTree::isSimpleRectangleCollision(sf::Vector2f A1, sf::Vector2f A2, sf::Vector2f B1, sf::Vector2f B2)
{
   return (A1.x < B2.x && A2.x > B1.x && A1.y < B2.y && A2.y > B1.y);
}

std::vector<int>
QuadTree::getPossibleCollisions(sf::Vector2f ul, sf::Vector2f lr)
{
	std::vector<int> list;

	if(hasChildren)
	{
		/*bool inChildren = false;

		for(int i = 0; i < CHILD_COUNT; ++i)
		{
			inChildren = children[i]->fitsInside(ul, lr);
			if(inChildren)
			{
				std::vector<int> childList = children[i]->getPossibleCollisions(ul, lr);
				for(int j = 0; j < childList.size(); ++j)
				{
					list.push_back(childList[j]);
				}
				break;
			}
		}

		if(!inChildren)
		{*/
			//grab all of children
			for(int i = 0; i < CHILD_COUNT; ++i)
			{
				if(isSimpleRectangleCollision(ul, lr, children[i]->upperLeft, children[i]->lowerRight))
				{
					//we intersect this child, so get the little fellows below
					std::vector<int> childList = children[i]->getPossibleCollisions(ul, lr);
					for(int j = 0; j < childList.size(); ++j)
					{
						list.push_back(childList[j]);
					}
				}
			}
		//}
	}

	for(int k = 0; k < objects.size(); ++k)
	{
		list.push_back(objects[k].id);
	}

	return list;
}