#include "polygon.hpp"

using namespace pct;

Polygon::Polygon()
{

}

Polygon::Polygon(std::vector<sf::Vertex>* newPoints, int newId)
{
	id = newId;
	float rectCorners[4] = {FLT_MIN, FLT_MAX, FLT_MIN, FLT_MAX};

	for(int i = 0; i < newPoints->size(); ++i)
	{
		sf::Vertex newP = newPoints->at(i);

		points.push_back(newP.position);
		renderPoints.push_back(newP);
		renderPoints.push_back(newPoints->at((i + 1) % newPoints->size()));

		//find rectangle corners
		if(newP.position.x > rectCorners[0]) rectCorners[0] = newP.position.x;
		if(newP.position.x < rectCorners[1]) rectCorners[1] = newP.position.x;
		if(newP.position.y > rectCorners[2]) rectCorners[2] = newP.position.y;
		if(newP.position.y < rectCorners[3]) rectCorners[3] = newP.position.y;
	}

	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[1], rectCorners[3]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[1], rectCorners[2]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[1], rectCorners[2]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[0], rectCorners[2]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[0], rectCorners[2]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[0], rectCorners[3]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[0], rectCorners[3]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[1], rectCorners[3]), sf::Color::Green));

	upperLeft = sf::Vector2f(rectCorners[1], rectCorners[3]);
	lowerRight = sf::Vector2f(rectCorners[0], rectCorners[2]);
}

Polygon::~Polygon()
{

}

void
Polygon::updateRenderPoints()
{
	float rectCorners[4] = {FLT_MIN, FLT_MAX, FLT_MIN, FLT_MAX};

	renderPoints.clear();
	renderRectPoints.clear();

	for(int i = 0; i < points.size(); ++i)
	{
		renderPoints.push_back(points[i]);
		renderPoints.push_back(points[(i + 1) % points.size()]);

		//find rectangle corners
		if(points[i].x > rectCorners[0]) rectCorners[0] = points[i].x;
		if(points[i].x < rectCorners[1]) rectCorners[1] = points[i].x;
		if(points[i].y > rectCorners[2]) rectCorners[2] = points[i].y;
		if(points[i].y < rectCorners[3]) rectCorners[3] = points[i].y;
	}

	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[1], rectCorners[3]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[1], rectCorners[2]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[1], rectCorners[2]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[0], rectCorners[2]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[0], rectCorners[2]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[0], rectCorners[3]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[0], rectCorners[3]), sf::Color::Green));
	renderRectPoints.push_back(sf::Vertex(sf::Vector2f(rectCorners[1], rectCorners[3]), sf::Color::Green));

	upperLeft = sf::Vector2f(rectCorners[1], rectCorners[3]);
	lowerRight = sf::Vector2f(rectCorners[0], rectCorners[2]);
}

void
Polygon::draw(sf::RenderWindow *window)
{
	//draw(window, false);
}

void
Polygon::draw(sf::RenderWindow *window, bool renderBoundingRect, sf::Transform globalTransform)
{
	window->draw(&renderPoints[0], renderPoints.size(), sf::Lines, globalTransform);
	if(renderBoundingRect) window->draw(&renderRectPoints[0], renderRectPoints.size(), sf::Lines);
}

void
Polygon::select()
{
	setColor(sf::Color::Yellow);
}

void
Polygon::deselect()
{
	setColor(sf::Color::Red);
}

void
Polygon::highlight()
{
	setColor(sf::Color::White);
}

void
Polygon::extraHighlight()
{
	setColor(sf::Color::Green);
}

void
Polygon::setColor(sf::Color newColor)
{
	for(int i = 0; i < renderPoints.size(); ++i)
	{
		renderPoints[i].color = newColor;
	}
}

sf::Vector2f
Polygon::getUpperLeft()
{
	return upperLeft;
}

sf::Vector2f
Polygon::getLowerRight()
{
	return lowerRight;
}