#ifndef POLY_POLYGON
#define POLY_POLYGON

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <vector>
#include <cfloat>

namespace pct
{

class Polygon
{
private:
	int id;
	std::vector<sf::Vertex> renderPoints;
	std::vector<sf::Vertex> renderRectPoints;
	sf::Vector2f upperLeft;
	sf::Vector2f lowerRight;

	void setColor(sf::Color newColor);

public:
	std::vector<sf::Vector2f> points;

	Polygon();
	Polygon(std::vector<sf::Vertex>* points, int id);
	~Polygon();

	void draw(sf::RenderWindow *);
	void draw(sf::RenderWindow *, bool, sf::Transform);

	sf::Vector2f getUpperLeft();
	sf::Vector2f getLowerRight();

	void select();
	void deselect();
	void highlight();
	void extraHighlight();

	void updateRenderPoints();
};

}

#endif